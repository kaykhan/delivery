const orders = require("./app/orders/order.route");

module.exports = function (app) {

    //resources
    app.use("/orders", orders);

    // health check
    app.get("/", (req, res, next) => {

        return res.status(200).json({
            success: true,
            message: "Server running successfully"
        });

    });


    // catch
    app.use("*", (req, res, enxt) => {

        return res.status(404).json({
            success: false,
            message: "Unrecognized Route"
        });

    });
};
