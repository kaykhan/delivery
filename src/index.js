const app = require("./app");
const routes = require("./routes")(app);
const config = require("./config");

const server = app.listen(config.port, config.url, err => {
    if (err) {
        process.exit(1);
    }

    console.log("Server running on:" + config.url + ":" + config.port);
});

module.exports = server;