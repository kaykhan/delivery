module.exports = {
    url: process.env.HOST,
    port: process.env.PORT || 8080,
    env: process.env.NODE_ENV,

    db: {
        mongodb: {
            uri:
                process.env.DB_HOST +
                ":" +
                process.env.DB_PORT +
                "/" +
                process.env.DB_NAME,
            options: {
                useNewUrlParser: true
                // 'user': process.env.DB_USERNAME,
                // 'pass': process.env.DB_PASSWORD,
            }
        }
    },
    GOOGLE_MAP_CLIENT_API_KEY: process.env.GOOGLE_MAP_CLIENT_API_KEY,
};
