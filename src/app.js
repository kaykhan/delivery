const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const app = express();


switch (process.env.NODE_ENV) {
    case "production":
        break;
    default:
        dotenv.load({ path: ".env" });
};

const config = require("./config");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));



mongoose.connect(config.db.mongodb.uri, config.db.mongodb.options).then(

    () => {
        console.log("MongoDB Successfully Connected On: " + config.db.mongodb.uri);
    },
    (err) => {
        console.error("MongoDB Error:", err);
        console.error("%s MongoDB connection error. Please make sure MongoDB is running.");
        throw err;
    },

);


module.exports = app;
