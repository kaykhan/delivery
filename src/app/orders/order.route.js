const express = require("express");
const OrderController = require("./order.controller.js");

const router = express.Router();
const controller = new OrderController();

router.get("/", controller.index.bind(controller));
// router.get("/:id", controller.show);
router.post("/", controller.store.bind(controller));
router.put("/:id", controller.update.bind(controller));
// router.delete("/:id", controller.destroy);

module.exports = router;
