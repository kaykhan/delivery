const Order = require("./order.model");
const OrderRepository = require("./order.repository");

class OrderController {

    constructor() {

        this.orderRepo = new OrderRepository();

    }

    async index(req, res, next) {

        const page = req.query.page || 1;
        const limit = req.query.limit || 10;

        const options = {
            page: parseInt(page, 10),
            limit: parseInt(limit, 10),
        }

        try {
            const orders = await Order.paginate({}, options);

            return res.status(200).json(orders.docs);
        } catch (err) {
            console.log(err);

            return res.status(500).json({ error: "ERROR_DESCRIPTION" });
        }
    }

    // async show(req, res, next) {}

    async store(req, res, next) {

        const input = req.body;

        const origin = { latitude: input.origin[0], longitude: input.origin[1] };
        const destination = { latitude: input.destination[0], longitude: input.destination[1] };

        let distance;
        try {

            distance = await this.orderRepo.calculateDistance(origin, destination);

        } catch (err) {
            console.log(err);
            return res.status(500).json({ error: "ERROR_DESCRIPTION" });
        }

        let order = new Order({
            origin: origin,
            destination: destination,
            distance: distance.value,
        });


        try {

            order = await order.save();

            const response = {
                id: order._id,
                distance: order.distance,
                status: order.status
            }

            return res.status(200).json(response);

        } catch (err) {

            console.log(err);
            return res.status(500).json({ error: "ERROR_DESCRIPTION" });

        }
    }

    async update(req, res, next) {

        const input = req.body;
        const id = req.params.id;

        try {

            let order = await Order.findById(id);

            if (!order) {

                return res.status(404).json({ error: "ORDER_NOT_FOUND" });
            }

            if (order.status === "taken") {

                return res.status(409).json({ error: "ORDER_ALREADY_BEEN_TAKEN" });
            }

            order.status = input.status;

            order = await order.save();

            return res.status(200).json({ status: "SUCCESS" });


        } catch (err) {

            console.error(err);
            return res.status(500).json({ error: "ERROR_DESCRIPTION" });

        }

    }

    // async destroy(req, res, next) {}


}

module.exports = OrderController;
