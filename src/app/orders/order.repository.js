const Google = require("./../../modules/google.module");

class OrderRepository {

    constructor() {

        this.googleMapsClient = Google.googleMapsClient;
    }

    async calculateDistance(origin, destination) {

        try {

            let distance;
            const query = {
                origins: [origin],
                destinations: [destination]
            }

            const matrix = await this.googleMapsClient.distanceMatrix(query).asPromise();

            if (matrix.status === 200) {

                distance = matrix.json.rows[0].elements[0].distance
            }

            return distance;


        } catch (err) {

            return err;

        }

    }

}

module.exports = OrderRepository;