
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const Schema = mongoose.Schema;

let OrderSchema = new Schema({
	status: {
		type: String,
		default: "UNASSIGN"
	},
	distance: {
		type: Number
	},
	origin: {
		latitude: {
			type: Number
		},
		longitude: {
			type: Number
		}
	},
	destination: {
		latitude: {
			type: Number
		},
		longitude: {
			type: Number
		}
	},
}, {

		timestamps: { createdAt: "created_at", updatedAt: "updated_at" }
	});

OrderSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Order", OrderSchema);
