const GoogleMapsCient = require("@google/maps");
const config = require("./../config");


let Google = {};

Google.googleMapsClient = GoogleMapsCient.createClient({
    key: config.GOOGLE_MAP_CLIENT_API_KEY,
    Promise: Promise
});

module.exports = Google;
