# Delivery


## Prerequisites

1. Docker

```
https://docs.docker.com/install/linux/docker-ce/ubuntu/
```


2. Docker Compose

```
https://docs.docker.com/compose/install/
```

3. Node

```
https://nodejs.org/en/
```

4. MongoDB (Optional Local Development)

```
https://www.mongodb.com/
```

## Getting Started

You can customise the environment variables such as the google maps api key. To do this you must modify the docker-compose.yml fle.

```
version: "2"
services:
  api:
    build: .
    environment:
      - NODE_ENV=production
      - HOST=0.0.0.0
      - PORT=8080
      - DB_HOST=mongodb://mongo
      - DB_PORT=27017
      - DB_NAME=delivery
      - GOOGLE_MAP_CLIENT_API_KEY=123 # Update This With Your Key!
    ports:
    - "8080:8080"
    depends_on:
    - mongo
  mongo:
    image: mongo
    ports:
    - "27017:27017"
```

The docker compose file will build and run the image from the DockerFile with the above configuration.



## Deployment

Run the start.sh file to install and initialise the docker image.

1. First give start.sh execute permission

```
chmod +x start.sh
```

2. Now execute the script

```
./start.sh
```

The API will now be available on `0.0.0.0:8080`

## Development

First make sure you have node and mongo running. Then execute the following commands.

```
npm install
npm run serve
```

## Test

To run tests  make sure you have no other conflicting servers running. Then execute the following commands.

```
npm install
npm run test
```

### Thoughts

There is more that could be done to make this a reliable production ready backend api. However, given the time and the task requirements i opted not to implement them.

1. Logging 
2. Process Management (PM2)
3. Cacheing (Redis)
4. Linting (Eslint)
5. Typescript!
6. PII Data Encryption

