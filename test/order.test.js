const server = require("./../src/index");
const mocha = require("mocha");
const chai = require("chai");
const request = require("supertest");

describe("Server", () => {

    it("should create an order", async () => {
        const body = {
            origin: [53.467297, -2.234659],
            destination: [53.462844, -2.230072]
        };

        const res = await request(server)
            .post("/orders")
            .send(body)
            .expect(200);
    });

    it("should have a correct distance value", async () => {
        const body = {
            origin: [53.467297, -2.234659],
            destination: [53.462844, -2.230072]
        };

        const res = await request(server)
            .post("/orders")
            .send(body)
            .expect(200);

        let expect = chai.expect;

        expect(res.body).to.have.property("distance");

        const distance = res.body.distance;

        expect(distance).to.equal(578);
    });

    it("should update an order to have the status be taken", async () => {
        const orders = await request(server).get("/orders");

        const order = orders.body[0]._id;

        const body = {
            status: "taken"
        };

        const res = await request(server)
            .put("/orders/" + order)
            .send(body)
            .expect(200);
    });

    it("should return 409 the order has already been taken", async () => {

        const orders = await request(server).get("/orders");

        const order = orders.body[0]._id;

        const body = {
            status: "taken"
        };

        const res = await request(server)
            .put("/orders/" + order)
            .send(body)
            .expect(409);
    });

    it("should return a list of orders", async () => {
        const res = await request(server)
            .get("/orders")
            .expect(200);
    });

});
