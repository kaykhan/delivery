const server = require("./../src/index");
const mocha = require("mocha");
const chai = require("chai");
const request = require("supertest");
const mongoose = require("mongoose");


before((start) => {
    console.log("Test Starting...");
    mongoose.connection.dropDatabase();
    start();
});

after((done) => {
    console.log("... Test Ended");
    mongoose.connection.dropDatabase();
    done();
})


describe("Server", () => {

    it("should be up", async () => {
        const resut = await request(server)
            .get("/")
            .expect(200);
    });

    it("should throw 404 unrecongized route", async () => {
        const result = await request(server)
            .get("/asdfg")
            .expect(404);
    });

});
